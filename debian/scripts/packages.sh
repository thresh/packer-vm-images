#!/bin/sh

set -e

PACKAGES="
vim
openssh-server
bzip2
curl
ca-certificates
lftp
telnet
openntpd
git-core
dirmngr
iftop
tcpdump
openntpd
"

cat << __EOF__ > /etc/apt/apt.conf.d/90local
APT::Install-Recommends "false";
APT::Install-Suggests "false";
__EOF__

apt-get update

apt-get install -y $PACKAGES

apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "deb https://apt.dockerproject.org/repo debian-stretch main" > /etc/apt/sources.list.d/docker.list

apt-get update

apt-get -y --force-yes install docker-engine

apt-get -y upgrade

# fix docker configs
mkdir -p /etc/systemd/system/docker.service.d/
cat > /etc/systemd/system/docker.service.d/override.conf << __EOF__
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:4243 -H unix:///var/run/docker.sock
# --storage-opt dm.basesize=20G
__EOF__
