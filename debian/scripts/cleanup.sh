#!/bin/sh

echo "unset old_host_name" > /etc/dhcp3/dhclient-enter-hooks.d/unset_old_hostname ||:
echo "unset old_host_name" > /etc/dhcp/dhclient-enter-hooks.d/unset_old_hostname ||:
rm -f /etc/hostname

# Cleanup log files
find /var/log -type f | while read f; do echo -ne '' > $f; done;

# remove under tmp directory
rm -rf /tmp/*

rm -f /etc/udev/rules.d/70-persistent-net.rules
rm /lib/udev/rules.d/75-persistent-net-generator.rules
rm -rf /dev/.udev/ /var/lib/dhcp/*
