PARTITIONS=vtbd0
DISTRIBUTIONS="base.txz kernel.txz lib32.txz"

#!/bin/sh
echo 'WITHOUT_X11="YES"' >> /etc/make.conf
echo 'nameserver 8.8.8.8' >> /etc/resolv.conf
cat >> /etc/rc.conf <<EOF
ifconfig_vtnet0="DHCP"
sshd_enable="YES"
devd_enable="YES"
dumpdev="AUTO"
EOF

env ASSUME_ALWAYS_YES=1 pkg bootstrap
pkg update
pkg install -y sudo
pkg install -y bash
pkg install -y curl

cat >/etc/devd/acpi.conf << __EOF__
notify 10 {
        match "system"          "ACPI";
        match "subsystem"       "Button";
        match "notify"          "0x00";
        action "/sbin/shutdown -p now";
};
__EOF__

chsh -s /usr/local/bin/bash root

echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo videolan | pw mod user root -h 0

reboot
