#!/usr/local/bin/bash

set -e

PACKAGES="
binutils
cmake
curl
gettext
git
gmake
libtool
libxml2
libxslt
libyaml
lua51
luajit
mercurial
openssl
pcre
png
rsync
subversion
wget
zsh
vim-lite
ffmpeg
tmux
doxygen
pkgconf
tcpdump
"

export ASSUME_ALWAYS_YES=yes
export DEFAULT_ALWAYS_YES=yes

pkg install $PACKAGES
